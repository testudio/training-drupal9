Drupal in Docker Containers

Part 1.
* What is container?
* Docker hub
* Docker container
* Docker ps vs docker images
* Composer container
* Drupal container
* Downloading container
* Executing container

Part 2.
Running complex commands in single container
Mounts and environmental variables
Database container
Static code analysis of the module with composer
Updating to Drupal 9

Part 3.
Scope of the container
Running containers vs container images
Testing your drupal 9 module
Drupal rector and Drupal check

Part 4.
Complex containers
Docker compose
Connecting Drupal to database
Adding database manager to container stac
Delete and restarting containers
Updating containers

Part 5.
Third party executors
Using containers in Gitlab CI
Using containers in Github Actions
Running container on digital ocean
Pushing your own container to container repository (docker hub, gitlab, digital ocean)