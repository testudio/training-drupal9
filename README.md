# Training Drupal9

### Running docker container with database.

Running drupal 9 with database on the same server.

```
docker-compose --file docker/docker-compose.db.yml up --detach --build
```
